#!/usr/bin/python
# -*- coding:utf-8; tab-width:4 -*-

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.    
"""


import os
import time
import sys
import thread
import threading
import logging
from copy import copy

import gtksourceview2
import gobject
import pango
import vte
try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
except:
	sys.exit(1)

import Interpreter
#import src.Interpreter

class InputThread(threading.Thread):
	stopthread = threading.Event()
	
	def __init__(self, widget):
		threading.Thread.__init__(self)
		self.widget = widget

	def stop(self):
		gobject.idle_add(self.widget.modify_base, self.widget.state,
						 gtk.gdk.Color(65535,65535,65535,0))

        self.stopthread.set()
		
	def run(self):
		white = True
		self.stopthread.clear()

		while not self.stopthread.isSet():
			if white:
				color = gtk.gdk.Color(65021,65535,48059,0)
			else:
				color = gtk.gdk.Color(65535,65535,65535,0)
									  				
			gobject.idle_add(self.widget.modify_base, self.widget.state, color)
			white = not white
			time.sleep(0.8)

class Gui:

	def __init__(self, file):
		self.widgets = gtk.glade.XML(file)
		self.widgets.signal_autoconnect(self)
		self.bufferPath = {}  # [sourceBuffer: path]
		self.bufferState = {}  # [sourceBuffer: modified]
		self.nb = self.widgets.get_widget("notebook")
		self.clipboard = gtk.Clipboard(gtk.gdk.display_get_default(),
					       "CLIPBOARD")

		self.terminal = self.buildTerminal()
		self.widgets.get_widget('viewportConsole').add(self.terminal)
		

		self.model = self.buildModel()
		self.viewTape = self.buildTapeView()

		self.widgets.get_widget('viewportTape').add(self.viewTape)

		win = self.widgets.get_widget("wMain")
		win.maximize()
		win.show_all()
		
		self.lockInput = threading.Condition()
		self.inputThread = None 
		
	def buildTerminal(self):
		terminal = vte.Terminal()
		terminal.set_scroll_background(True)
		terminal.set_colors(gtk.gdk.Color(0, 0, 0, 0),
								 gtk.gdk.Color(65535, 65535, 65535, 0), [])
		terminal.set_color_highlight(gtk.gdk.Color(30000, 30000, 30000, 0))
		return terminal

	def buildModel(self):
		columns = [str] * 400 
		retval = gtk.ListStore(*columns)
		initials = [0] * 400
		retval.append(initials)
		return retval

	def buildTapeView(self):
		retval = gtk.TreeView(self.model)

		renderer = gtk.CellRendererText()	
		renderer.set_fixed_size(25,-1)
		# renderer.set_property("background-gdk", gtk.gdk.Color(0, 0, 0, 0))
# 		renderer.set_property("foreground-gdk", gtk.gdk.Color(65535, 65535, 65535, 0))
		# renderer.set_property("family-set", True)
		renderer.set_property("font", "monospace 10")
		renderer.set_property("alignment", pango.ALIGN_CENTER)

		for i in range(400):
			tvc = gtk.TreeViewColumn(str(i), renderer, text=i)	
			retval.append_column(tvc)
			tvc.set_alignment(0.5)
		return retval
			
	def __put_language(self, sbuffer):
		lm = gtksourceview2.LanguageManager()
		language = lm.get_language('bf')

		if language:
			sbuffer.set_highlight_syntax(True)
			sbuffer.set_highlight_matching_brackets(False)
			sbuffer.set_language(language)
		else:
			sbuffer.set_highlight_syntax(False)

	def changed(self, boffer):
		if not self.bufferState[boffer]:
			self.bufferState[boffer] = True
			self.nb.set_tab_label_text(boffer,
									'*' + self.nb.get_tab_label_text(boffer))
			# self.bufferPath[boffer].set_text(
# 				'*' + self.bufferPath[boffer].get_text())

	def wMain_delete_event(self, widget, event, data=None):
		if self.inputThread:
			self.inputThread.stop()

		gtk.main_quit()

	def new_clicked(self, widget, data=None):
		index = self.nb.get_current_page()
		logging.info("Current Page: " + str(index))
		logging.info("Buffer states: " + str(self.bufferState))

		if index != -1 and \
			self.bufferState[self.nb.get_nth_page(index).get_buffer()]:
				self.widgets.get_widget("wSaveChanges").show()
		else:
			self.nb.remove_page(0)
			sourceView = gtksourceview2.View(gtksourceview2.Buffer())
			label = gtk.Label("Documento no guardado") 
			self.bufferPath[sourceView.get_buffer()] = ""
			self.nb.append_page(sourceView, label)
			self.nb.show_all()
			self.__put_language(sourceView.get_buffer())
			self.bufferState[sourceView.get_buffer()] = False				
			fuente = pango.FontDescription("monospace 10")
			sourceView.modify_font(fuente)
			sourceView.get_buffer().connect('mark-set', self.changed)

	def buttonCancel_clicked(self, widget, data=None):
		self.widgets.get_widget("wSaveChanges").hide()
			
	def buttonCloseWChanges_clicked(self, widget, data=None):
		self.widgets.get_widget("wSaveChanges").hide()
		self.nb.remove_page(0)
			
	def save_clicked(self, widget, data=None):
		index = self.nb.get_current_page()
		buf = self.nb.get_nth_page(index).get_buffer()
		a = "prueba"
		if self.bufferPath[buf] == "":
				self.saveAs_clicked(widget, data=None)
		else:
				try:
						fescritura = open(self.bufferPath[buf],'w')
						fescritura.write(buf.get_text(
								buf.get_buffer().get_start_iter(),
								buf.get_buffer().get_end_iter(),
								True) + "\n")
						self.bufferState[buf] = False
						self.nb.set_tab_label_text(buf,
									self.bufferPath[buf].split("/")[-1])
				except IOError :
						logging.warning("File does not exist.")
				fescritura.close()

	#ToolBar
	def lopen_show_menu(self, widget, data=None):
		print "menu"

	#def toolItemPrint(self,widget,data=None):

	def toolItemExecute_clicked(self, widget, data=None):
		int = Interpreter.Interpreter()
		int.inputMgr.getLock = self.getLockInput

		iw = self.widgets.get_widget('entryInput')
		iw.set_text("")
		int.inputMgr.widget = iw

		showTape = self.widgets.get_widget('expanderTape').get_expanded()
		#int = Interpreter.Interpreter()
		
		self.model.clear()
		self.model.append(['0']*400)
		
		try:
		    buf = self.nb.get_nth_page(self.nb.get_current_page()).get_buffer()
			int.program = buf.get_text(buf.get_start_iter(),
										buf.get_end_iter())
		except IndexError:
			logging.warning("There is not input file.")
			return
		
		self.cleanTerminal(None)
		logging.info("Starting interpreter thread")

		if showTape:
			int.notify = self.notify
		
		thread.start_new_thread(int.run, (self.writeToTerminal,))
		time.sleep(0.01)
		logging.info("Starting progress thread")
		thread.start_new_thread(self.progress, (int,widget))

	def notify(self, pointer, value):
		it = self.model.get_iter_first()
		try:
			self.model.set_value(it, pointer, value)
		except ValueError:
			logging.error("Maximum cell number reached")
		

	def writeToTerminal(self, char):
		msg = char
		if char == "\n":
			msg = char + "\r"
		self.terminal.feed(msg, len(msg))
			
	def getLockInput(self):
		self.inputThread = InputThread(self.widgets.get_widget('entryInput')) 
		self.inputThread.start()	
		logging.info("Starting input thread")
		return self.lockInput
	
	def input_clicked_cb(self, widget):
		self.lockInput.acquire()
		if self.inputThread:
			self.inputThread.stop()
		self.lockInput.notifyAll()
		self.lockInput.release()
		
	def entryInput_focus_in_event_cb(self, widget, event):
		if self.inputThread:
			self.inputThread.stop()

	def writeToTape(self, interpreter, it):
			pass
			
	def progress(self, interpreter, widget):
		widget.set_sensitive(False)
		#Wait for the end of the interpreter
		interpreter.lock.acquire()
		widget.set_sensitive(True)
		logging.info("Stopping progress thread")
		interpreter.lock.release()

	def toolItemExecuteStep_clicked(self, widget, data=None):
		index = self.nb.get_current_page()	
		buf = self.nb.get_nth_page(index).get_buffer()
		self.int.program = buf.get_text(
			buf.get_start_iter(),
			buf.get_end_iter(), True)
		self.int.next()

	def search_clicked(self, widget, data=None):
		self.widgets.get_widget("wSearch").show()

	def buttonSearch_clicked(self, widget, data=None):
		tb = self.widgets.get_widget("entrytb").get_text()
		index = self.nb.get_current_page()	
		buf = self.nb.get_nth_page(index).get_buffer()
		pos = buf.get_text(
			buf.get_start_iter(),
			buf.get_end_iter(),
			True).find(tb)

	def replace_clicked(self, widget, data=None):
		self.widgets.get_widget("wReplace").show()

	def buttonReplace_clicked(self, widget, data=None):
		tar = self.widgets.get_widget("entrytar").get_text()
		tcr = self.widgets.get_widget("entrytcr").get_text()

	def buttonReplaceAll_clicked(self, widget, data=None):
		tar = self.widgets.get_widget("entrytar").get_text()
		tcr = self.widgets.get_widget("entrytcr").get_text()

	#def buttonSkip_clicked(self,widget,data=None):

	def cancel_clicked(self, widget, data=None):
		self.widgets.get_widget("wSearch").hide()

	def buttonReplace_clicked(self, widget, data=None):
		self.widgets.get_widget("wReplace").show()

	#Buttons of MenuBar
	def open_clicked(self, widget, data=None):
		index = self.nb.get_current_page()
		if index != -1 and \
			self.bufferState[self.nb.get_nth_page(index).get_buffer()]:
				self.widgets.get_widget("wSaveChanges").show()
		else:
				dialog = gtk.FileChooserDialog("Abrir archivo", None,
										gtk.FILE_CHOOSER_ACTION_OPEN,
										(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
										gtk.STOCK_OPEN, gtk.RESPONSE_OK))
				dialog.set_default_response(gtk.RESPONSE_OK)
				response = dialog.run()
				if response == gtk.RESPONSE_OK:
						try:
								self.nb.remove_page(0)
								filen = dialog.get_filename()
								sourceView = gtksourceview2.View(
										gtksourceview2.Buffer())
								buf = sourceView.get_buffer()
								label = gtk.Label(filen.split("/")[-1])
								self.bufferPath[buf] = filen
								self.nb.append_page(sourceView, label)
								self.nb.show_all()
								self.__put_language(sourceView.get_buffer())
								self.bufferState[buf] = False
								flectura=open(filen,'r')
								fdecode = flectura.read().decode('ascii')
								bencode = fdecode.encode('utf8')
								buf.set_text(bencode)
								fuente = pango.FontDescription("monospace 10")
 								sourceView.modify_font(fuente)
								buf.connect('changed', self.changed)
						except IOError :
								logging.warning("File does not exist.")
						flectura.close()
				elif response == gtk.RESPONSE_CANCEL:
						logging.info("There are not selected elements.")
				dialog.destroy()

	def close_activate(self, widget, data=None):
		self.nb.remove_page(self.nb.get_current_page())

	def closeAll_activate(self, widget, data=None):
		for i in range(self.nb.get_n_pages()):
			# When a page is deleted, all the pages are renumerated	
			self.nb.remove_page(0)  

	def saveAs_activate(self, widget, data=None):
		dialog = gtk.FileChooserDialog("Guardar archivo", None,
						gtk.FILE_CHOOSER_ACTION_SAVE, (gtk.STOCK_CANCEL,
						gtk.RESPONSE_CANCEL,gtk.STOCK_SAVE, gtk.RESPONSE_OK))
		dialog.set_default_response(gtk.RESPONSE_OK)
		response = dialog.run()
		if response == gtk.RESPONSE_OK:
		    index = self.nb.get_current_page()
			buf = self.nb.get_nth_page(index).get_buffer()	
			try:
				filen = dialog.get_filename()
				fescritura=open(filen,'w')
				self.bufferPath[buf] = filen
				fescritura.write(buf.get_text(
					buf.get_start_iter(),
					buf.get_end_iter(), True) + "\n")
				self.bufferState[currentTab.get_buffer()] = False
			except IOError :
				logging.warning("File does not exist.")
			fescritura.close()
		elif response == gtk.RESPONSE_CANCEL:
			logging.info("There are not selected elements.")
		dialog.destroy()

	def undo_activate(self, widget, data=None):
		index = self.nb.get_current_page()
		self.nb.get_nth_page(index).get_buffer().undo()	

	def redo_activate(self, widget, data=None):
		index = self.nb.get_current_page()
		self.nb.get_nth_page(index).get_buffer().redo()

	def cut_activate(self, widget, data=None):
		index = self.nb.get_current_page()
		buf = self.nb.get_nth_page(index).get_buffer()
		buf.cut_clipboard(self.clipboard, True)

	def copy_activate(self, widget, data=None):
		index = self.nb.get_current_page()
		buf = self.nb.get_nth_page(index).get_buffer()
		buf.copy_clipboard(self.clipboard)

	def paste_activate(self, widget, data=None):
		index = self.nb.get_current_page()
		buf = self.nb.get_nth_page(index).get_buffer()
		buf.paste_clipboard(self.clipboard, None, True)

	def delete_activate(self, widget, data=None):
		index = self.nb.get_current_page()
		buf = self.nb.get_nth_page(index).get_buffer()	
		buf.set_buffer(gtksourceview2.Buffer())

	#def selectAll_activate(self,widget,data=None):

	def preferences_activate(self, widget, data=None):
		self.widgets.get_widget("dialogPreferences").show()

	def quit_activate(self, widget, data=None):
		self.widgets.get_widget("wMain").destroy()
		gtk.main_quit()

	def about_activate(self, widget, data=None):
		about = self.widgets.get_widget("dialogAbout")
		about.run()
		about.hide()

	# WindowPreferences
	def numberLine_toggled(self, widget, data=None):
		if self.widgets.get_widget("numberLinecheckbutton").get_active():
			for e in self.nb.get_children():
				e.set_show_line_numbers(True)
		else:
			for e in self.nb.get_children():
				e.set_show_line_numbers(False)

	def currentLine_toggled(self, widget, data=None):
		if self.widgets.get_widget("currentLinecheckbutton").get_active():
			for e in self.nb.get_children():
				e.set_show_line_marks(True)
		else:
			for e in self.nb.get_children():
				e.set_show_line_marks(False)

	def rightMargin_toggled(self, widget, data=None):
		if self.widgets.get_widget("rightMargincheckbutton").get_active():
			for e in self.nb.get_children():
				e.set_show_right_margin(True)
		else:
			for e in self.nb.get_children():
				e.set_show_rigth_margin(False)

	def close_clicked(self,widget,data=None):
		self.widgets.get_widget("dialogPreferences").hide()

	#WindowAbout
	def buttonOK_clicked(self, widget, data=None):
		self.widgets.get_widget("dialogAbout").hide()

	def cleanTerminal(self, widget):
		logging.info("Terminal cleaned")
		self.terminal.reset(True, True)

	def setOpacity(self, widget, event):		
		logging.info("Opacity changed to: " + str(int(widget.get_value())))
		self.terminal.set_opacity(int(widget.get_value()))
			
    def main(self):
		gtk.main()

if __name__ == "__main__":
	gtk.gdk.threads_init()

	logging.info("Loading glade file")
	app = Gui("dotbf.glade")

	logging.info("Launching application")
	app.main()

