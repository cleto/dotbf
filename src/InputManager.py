# -*- coding: utf-8 -*-

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.    
"""

import sys

class InputManager:
    def __init__(self, input = None):
        self.input = input
        self.p = 0
        
    def getChar(self):
        if self.input:
            if self.p < len(self.input):
                char = ord(self.input[self.p])
                self.p += 1
                return char
            
            self.p = 0
            self.input = ""
            return 0
        
        self.input = self.getStream()
        self.p = 1
        return ord(self.input[0])
        
    def getStream(self):
        abstract

class TerminalInputManager(InputManager):
    
    def __init__(self):
        InputManager.__init__(self)
        
        self.terminal = sys.stdin
        
    def getStream(self):
        return self.terminal.read()
    
class WidgetInputManager(InputManager):
    
    def __init__(self, widget, cb):
        InputManager.__init__(self)
        self.getLock= cb
        self.widget = widget

    def getStream(self):
        lock = self.getLock()
        lock.acquire()
        lock.wait()
        text = self.widget.get_text()
        lock.release()
        return text

       
