# -*- coding: utf-8 -*-
"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

#Interpreter class

import sys
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    filename='.dotBF.log',
                    filemode='w',
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S')
        
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(logging.Formatter('%(levelname)s[%(name)s]: %(message)s'))
logging.getLogger().addHandler(console)

try:
    import psyco
    psyco.full()
except:
    logging.warning("psyco not found. Install it for a better performance. Forget this message if you use a PowerPC machine :-)")
    
import threading
from threading import Thread

import InputManager

class Interpreter(Thread):
    stopthread = threading.Event()
    
    def __init__(self, inputMgr=None, notify = None, program=None):
        
        Thread.__init__(self)
        
        self.pc = 0
        self.p = 0
        self.t = [0]
        self.inputMgr = InputManager.WidgetInputManager(None, None)
        self.output = ""
        self.notify = notify        

        self.commands = {"+":self.plus, "-":self.minor, "<":self.lessThan,
                         ">":self.greaterThan, ".":self.dot, ",":self.comma,
                         "[":self.openBracket, "]":self.closeBracket}

        self.lock = threading.Lock()

        self.program = ""

        if program:
            self.program = program
                    
    # @program is an integer list
            
    def addCommand(self, symbol, function):
        if symbol in self.commands.keys():
            return 
        self.commands[symbol] = function

    def stop(self):
        self.stopthread.set()

    def run(self, write):
        self.lock.acquire()
        if self.program:
            while self.pc < len(self.program):
                out = self.next()
                if out:
                    write(out)

                time.sleep(0)

        self.lock.release()

    def next(self):
        retval = None

        try:
            retval = self.commands[self.program[self.pc]]()
        except KeyError:
            retval = ""
            
        if not retval:
            self.pc += 1
            return ""
        return retval


    def plus(self):
        if self.t[self.p] >= 255:
            self.t[self.p] = 0
        else:
            self.t[self.p] += 1        

        if self.notify:
            self.notify(self.p, self.t[self.p])

    def minor(self):
        if self.t[self.p] == 0:
            self.t[self.p] = 255
        else:
            self.t[self.p] -= 1

        if self.notify:
            self.notify(self.p, self.t[self.p])
    
    def greaterThan(self):
        self.p += 1
        try:
            self.t[self.p]
        except IndexError:
            self.t.append(0)
    
    def lessThan(self):
        if self.p == 0:
            self.t.insert(0,0)
        else:
            self.p -= 1
    
    def dot(self):
        self.pc += 1
        return chr(self.t[self.p])

    def comma(self):
        self.t[self.p] = self.inputMgr.getChar()
   
    def openBracket(self):
        if self.t[self.p] == 0:
            count = 1              
            while 1:
                self.pc += 1
                try:
                    j = self.program[self.pc]
                except IndexError:
                    logging.error("HOYGAN!")
                if j == "[":
                    count += 1
                elif j == "]":
                    count -= 1
                    if count == 0:
                        break


    
    def closeBracket(self):
        if self.t[self.p] != 0:
            count = 1
            while 1:
                self.pc -= 1
                try:
                    j = self.program[self.pc]
                except IndexError:
                    logging.error("HOYGAN!")
                if j == "]":
                    count += 1
                elif j == "[":
                    count -= 1
                    if count == 0:
                        break
   

    def reset():
        self.pc = 0
        self.p = 0
        self.t = [0]
        self.input = ""
        self.output = ""
                         
if __name__ == "__main__":
    try:
        i=Interpreter( program = open(sys.argv[1]).read())
        i.inputMgr = InputManager.TerminalInputManager()
    except Exception, e:
        logging.error(e)
        sys.exit(0)
    i.run(sys.stdout.write)


